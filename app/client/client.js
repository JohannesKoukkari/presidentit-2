function loadApp() {
  console.log("loading application");

  // Bind AJAX call to the click event of Button #lataa
  $('#lataa').click(function(event) {
    // TODO: check estää tapahtumasta
    event.preventDefault();

    // The server must be bind to localhost (for testing) as we don't have a FQDN or HTTP proxy available
    //TODO: correct URI check
    $.get("http://localhost:8080/api/v1/presidents", function(data) {
      console.log("Sending HTTP GET to server");
    })
    .done(function( data ) {
      console.log("response from server :", data);
      //TODO: esitä vastaanotettu data webbisivulla, eli 
      // rakenna tässä kohtaa taulukko table+th+tr+td-elementein
      // palvelimelta tuleva vastaus on data-muuttujassa JavaScript-oliona
      let presidents = document.getElementById('presidentit').innerHTML = "<p>" + JSON.stringify(data) + "</p>";
    })
    .fail(function(err) {
      console.log("error");
    })
    .always(function() {
      console.log("finished");
    });
  });
}