var mongoose  = require('mongoose');

// TODO: define the president schema

// i.e. structure of the MongoDB document
var schema = mongoose.Schema({
  firstname : { 
    type: String,
    required: true, 
    max: 50
  },
  lastname : { 
    type: String,
    required: true, 
    max: 50
  },
  nickname : { 
    type: String,
    required: true, 
    max: 15, 
    validate: function(nickname) {
      return /[A-Za-z]/.test(nickname);
    }
  },
  born : {
    type: Number,
    required: true,
    min: 0
  },
  died : {
    type: Number,
    required: false
  },
  img : {
    type: String,
    required: true
  },
  career : {
    type: String,
    required: true
  },
  biography: {
    type: String,
    required: true
  }
  
  // TODO: check lisää loput presidentteihin liittyvät asiat
});
/*
new president({firstname: "Kaarlo Juho", lastname: "Ståhlberg", nickname: "Juho", born: "1865", died: "1952", img: "https://kansallisbiografia.fi/img/kb/0062604.jpg", career: "1919 - 1925", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/626"});
db.presidents.insert({firstname: "Lauri Kristian", lastname: "Relander", nickname: "Lauri", born: "1883", died: "1942", img: "https://kansallisbiografia.fi/img/kb/0062804.jpg", career: "1925 - 1931", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/628"});
db.presidents.insert({firstname: "Pehr Evind", lastname: "Svinhufvud", nickname: "Ukko-Pekka", born: "1861", died: "1944", img: "https://kansallisbiografia.fi/img/kb/0050104.jpg", career: "1931 - 1937", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/501"});
db.presidents.insert({firstname: "Kyösti", lastname: "Kallio", nickname: "Kyösti", born: "1873", died: "1940", img: "https://kansallisbiografia.fi/img/kb/0062905.jpg", career: "1937 - 1940", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/629"});
db.presidents.insert({firstname: "Risto Heikki", lastname: "Ryti", nickname: "Risto", born: "1889", died: "1956", img: "https://kansallisbiografia.fi/img/kb/0063004.jpg", career: "1940 - 1944", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/630"});
db.presidents.insert({firstname: "Carl Gustaf Emil", lastname: "Mannerheim", nickname: "Gustaf", born: "1867", died: "1951", img: "https://kansallisbiografia.fi/img/kb/0062508.jpg", career: "1944 - 1946", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/625"});
db.presidents.insert({firstname: "Juho Kusti", lastname: "Paasikivi", nickname: "Juho", born: "1870", died: "1956", img: "https://kansallisbiografia.fi/img/kb/0063107.jpg", career: "1946 - 1956", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/631"});
db.presidents.insert({firstname: "Urho Kaleva", lastname: "Kekkonen", nickname: "Urho", born: "1900", died: "1986", img: "https://kansallisbiografia.fi/img/kb/0063202.jpg", career: "1956 - 1981", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/632"});
db.presidents.insert({firstname: "Mauno Henrik", lastname: "Koivisto", nickname: "Mauno", born: "1923", died: "2017", img: "https://kansallisbiografia.fi/img/kb/0063302.jpg", career: "1981 - 1994", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/633"});
db.presidents.insert({firstname: "Martti", lastname: "Ahtisaari", nickname: "Martti", born: "1937", died: " ", img: "https://kansallisbiografia.fi/img/kb/0063401.jpg", career: "1994 - 2000", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/634"});
db.presidents.insert({firstname: "Tarja", lastname: "Halonen", nickname: "Tarja", born: "1943", died: " ", img: "https://kansallisbiografia.fi/img/kb/0406401.jpg", career: "2000 - 2012", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/4064"});
db.presidents.insert({firstname: "Sauli", lastname: "Niinistö", nickname: "Sauli", born: "1948", died: " ", img: "https://kansallisbiografia.fi/img/kb/0882101.jpg", career: "2012 - ", biography: "https://kansallisbiografia.fi/kansallisbiografia/henkilo/8821"});
*/

// create the model for presidents and expose it to the application
module.exports = mongoose.model('President', schema);