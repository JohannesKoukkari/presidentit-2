const express = require('express');
const Database = require('./database.js');

const connection = new Database();
const app = express();
const port = 8080;
app.use(express.json());
var cors = require('cors');

// CORS-middlewaren käyttöönotto
app.use(cors());
// Sallitaan pääsy selaimen tarvitsemiin tiedostoihin
app.use(express.static(__dirname+'/client')); 

app.get('/api/v1/presidents', (req, res) => {
  console.log("GET /api/v1/presidents");
  // TODO: tee mongoose.js-kirjastoa käyttäen kysely mongodb-tietokantaan
  res.send("TODO: tee kysely tietokantaan", presidents);
  
connection.getAll(function(callback){
res.send(callback);
 })
});

app.get('api/v1/presidents', (req, res) => {
  console.log("GET api/v1/presidents");
  res.send("editoi tarvittava REST-resurssi client.js-tiedostoon");
})

app.listen(port, '0.0.0.0', () => console.log(`Example app listening on port ${port}!`));