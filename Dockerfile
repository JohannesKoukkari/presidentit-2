FROM ubuntu:18.04

WORKDIR /app
COPY ./app /app

RUN apt update && apt upgrade
RUN apt install nodejs npm -y

EXPOSE 8080

CMD ["node", "server.js"]
